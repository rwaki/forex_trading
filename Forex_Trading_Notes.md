# Forex Trading

We are in  "Central Daylight Time" (CDT) and is UTC−05:00

<pre><code>This is an example code block.
</code></pre>

The 3 Finger Method Rules:-

* Breakout to the TOP we are looking for a SELL*  
* Breakout to the BOTTOM we are looking for a BUY*
 
Step 1 – Locate the head. Find a breakout to the top by finding the HIGHEST candle or find a breakout to the bottom by finding the LOWEST candle. Look left and look right. Make sure there is no candle/candles higher for breakouts to the top or lower for breakouts to the bottom.
 
Step 2 – Locate the swing point or points that swung into the head. The swing point will be the OPPOSITE COLOR of the head and will be on the left side of the head.
 
Step 3 – Locate your top left shoulder. The top left shoulder will be THE SAME COLOR as your head. The top left shoulder is the single candle BEFORE the swing point or points.  
 
Step 3.5 – Box off the top left shoulder. Box off the WICKS first, on top and bottom. Box off the BOTTOM of the candle body for a better retracement.
 
Step 4 – Locate the BOTTOM line of your top left shoulder. Follow the BOTTOM line going to the RIGHT (pass the head) to locate your 100% BODY retracement. (Cannot be a wick)
 
Step 4.5 – Follow the line that is on the BOTTOM BODY of your top left shoulder, going to the RIGHT (pass the head) to locate a better 100% BODY retracement. (Cannot be a wick)
 
Step 5 – From your 100% retracement candle, go up to your TOP line. Follow the TOP line going to the right, and anything that touch the line rather a BODY candle or a WICK, will be your buy or sell entry creating your top right shoulder.
 
*ON INVERTED HEAD AND SHOULDER YOUR TOP AND BOTTOM LINES ARE REVERSED *


##LIMITS

BUY Limit  :found a market that was going down but you supsect a point inn which the market will start to buy i.e a u turn for the ground  
SELL limit: found a market that was going up but you suspected a point in which the market was going to start selling  


## [Forwarded from FREEL$FE101 RESOURCES📚]  
## BEST TIMES TO TRADE: EST
    3am-4am
    8a-11a
    
    London 3a-11a
    EUR/USD
    AUD/USD
    GBP/USD
    NZD/USD
    USD/JPY
    USD/CAD
    USD/CHF
    GBP/JPY
    EUR/JPY
    EUR/GBP
    
    New York 8a-5p
    EUR/USD
    GBP/JPY
    GBP/USD
    AUD/JPY
    AUD/USD
    EUR/JPY
    USD/CAD
    NZD/USD
    
    Sydney 3p-12a
    EUR/AUD
    AUD/JPY
    NZD/USD
    NZD/JPY
    
    Tokyo 7p-4a
    GBP/JPY
    EUR/JPY
    AUD/USD
    
## Basic Training

### Practice chart 1


![Chart1](https://gitlab.com/rwaki/forex_trading/-/raw/5b76e40c547a38f401ddf97053697fc38faf514c/ForexChart1.png)

### Practice chart 2

![Chart1](https://gitlab.com/rwaki/forex_trading/-/raw/master/ForexChart2.png)